package andriboom.tbotubes;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import java.util.ArrayList;
import java.util.Locale;
import andriboom.tbotubes.assets.TranslateBackgroundTask;

import static android.speech.RecognizerIntent.ACTION_RECOGNIZE_SPEECH;
import static android.speech.RecognizerIntent.EXTRA_PREFER_OFFLINE;

public class HomeActivity extends AppCompatActivity {
    private static final int CODE_SPEACH_INPUT = 69;
    private static final int PERMISSIONS_REQUEST_RECORD_AUDIO = 1;
    private static final String ID_EN_TITLE = "Terjemahan<br>Indonesia &#10132 Inggris";
    private static final String ID_EN_BTN_CHANGE = "Indonesia &#10132 Inggris";
    private static final String ID_EN_BTN_PLAY = "Putar Kembali";

    private static final String EN_ID_TITLE = "Translate<br>English &#10132 Indonesia";
    private static final String EN_ID_BTN_CHANGE = "English &#10132 Indonesia";
    private static final String EN_ID_BTN_PLAY = "Play Again";

    private Context context;
    private EditText edtTextSpeak;
    private FloatingActionButton btnSpeak;
    private CardView btnPlay,btnTrasnlateChange,btnSpeak2;
    private TextView tvPlay,tvTranslateChange,tvTittleHeader;
    private TextToSpeech textToSpeech;
    private SpeechRecognizer speechRecognizer;
    private Intent intentSpeech;
    public boolean IDtoEN = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        initComponent();
    }

    private void setUpSpeak(final String text){
        textToSpeech = new TextToSpeech(context, new TextToSpeech.OnInitListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onInit(int status) {
                if (status==TextToSpeech.SUCCESS){
                    if (IDtoEN){
                        textToSpeech.setLanguage(Locale.US);
                        textToSpeech.setPitch(1.1f); // saw from internet
                        textToSpeech.setSpeechRate(0.5f);// f denotes float, it actually type casts 0.5 to float
                    }else{
                        textToSpeech.setLanguage(new Locale("ind"));
                        textToSpeech.setPitch(1.0f); // saw from internet
                        textToSpeech.setSpeechRate(1.0f);// f denotes float, it actually type casts 0.5 to float
                    }
                    textToSpeech.setPitch(1.1f); // saw from internet
                    textToSpeech.setSpeechRate(0.5f);// f denotes float, it actually type casts 0.5 to float
                    textToSpeech.speak(text,TextToSpeech.QUEUE_FLUSH,null);
                }
            }
        });
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initComponent() {
        context = this;
        //speecj setup

        speechRecognizer = SpeechRecognizer.createSpeechRecognizer(context);
        intentSpeech = new Intent(ACTION_RECOGNIZE_SPEECH);
        intentSpeech.putExtra(RecognizerIntent.EXTRA_LANGUAGE,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intentSpeech.putExtra(EXTRA_PREFER_OFFLINE,true);
        speechRecognizer.setRecognitionListener(new RecognitionListener() {
            @Override
            public void onReadyForSpeech(Bundle params) {

            }

            @Override
            public void onBeginningOfSpeech() {

            }

            @Override
            public void onRmsChanged(float rmsdB) {

            }

            @Override
            public void onBufferReceived(byte[] buffer) {

            }

            @Override
            public void onEndOfSpeech() {
            }

            @Override
            public void onError(int error) {
                if (error==SpeechRecognizer.ERROR_NO_MATCH){
                    Toast.makeText(context, "Tidak mengerti, coba ucapkan lagi", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(context, "microphone sedang digunakan", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onResults(Bundle results) {
                ArrayList<String> matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                if (matches!=null){
                    edtTextSpeak.setText(matches.get(0));
                    translateTextProcess(matches.get(0),IDtoEN);
                }else {
                    Toast.makeText(context, "Tidak mengerti, coba ucapkan lagi", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onPartialResults(Bundle partialResults) {

            }

            @Override
            public void onEvent(int eventType, Bundle params) {

            }
        });
//        init view
        tvTittleHeader = findViewById(R.id.tv_title_header);
        edtTextSpeak = findViewById(R.id.edt_text_speak);

        btnSpeak = findViewById(R.id.btn_speak);
        btnSpeak2 = findViewById(R.id.btn_speak2);
        btnTrasnlateChange = findViewById(R.id.btn_translate_change);
        tvTranslateChange = findViewById(R.id.tv_translate_change);
        btnPlay = findViewById(R.id.btn_play);
        tvPlay = findViewById(R.id.tv_play);
        btnPlay.setVisibility(View.GONE);
        tvTittleHeader.setText(Html.fromHtml(ID_EN_TITLE));
        tvPlay.setText(Html.fromHtml(ID_EN_BTN_PLAY));
        tvTranslateChange.setText(Html.fromHtml(ID_EN_BTN_CHANGE));
        btnTrasnlateChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (IDtoEN){
                    tvTittleHeader.setText(Html.fromHtml(EN_ID_TITLE));
                    tvPlay.setText(Html.fromHtml(EN_ID_BTN_PLAY));
                    tvTranslateChange.setText(Html.fromHtml(EN_ID_BTN_CHANGE));
                    IDtoEN = false;
                    intentSpeech.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, Locale.ENGLISH);
                }else{
                    tvTittleHeader.setText(Html.fromHtml(ID_EN_TITLE));
                    tvPlay.setText(Html.fromHtml(ID_EN_BTN_PLAY));
                    tvTranslateChange.setText(Html.fromHtml(ID_EN_BTN_CHANGE));
                    IDtoEN = true;
                    intentSpeech.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "id-ID");
                }
            }
        });

        btnSpeak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                speak();
            }
        });
        btnSpeak2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                speak();
            }
        });
    }

    private boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, PERMISSIONS_REQUEST_RECORD_AUDIO);
            return false;
        }else {
            return true;
        }
    }

    private void speak(){
        if (checkPermission()){
            Toast.makeText(context, "Ucapkan sesuatu...", Toast.LENGTH_SHORT).show();
            speechRecognizer.startListening(intentSpeech);
        }else{
            Toast.makeText(context, "access denied", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CODE_SPEACH_INPUT) {
            if (resultCode == RESULT_OK && (data != null)) {
                ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                if (result==null || result.isEmpty()){
                    Toast.makeText(context, "Tidak mengerti, coba ucapkan lagi", Toast.LENGTH_SHORT).show();
                }else{
                    edtTextSpeak.setText(result.get(0));
                    translateTextProcess(result.get(0), IDtoEN);
                }
            }
        }else{
            Toast.makeText(context, "Tidak mengerti, coba ucapkan lagi", Toast.LENGTH_SHORT).show();
        }
    }

    private void translateTextProcess(String queryText, boolean IDEN) {
        String languagePair;
        if (IDEN){
            languagePair = "id-en";
        }else{
            languagePair = "en-id";
        }
        TranslateBackgroundTask translatorBackgroundTask= new TranslateBackgroundTask(context,this);
        String translationResult = "";
        translatorBackgroundTask.execute(queryText,languagePair);
    }

    @Override
    protected void onPause() {
        if (textToSpeech!=null){
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (textToSpeech!=null){
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
        super.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case PERMISSIONS_REQUEST_RECORD_AUDIO:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkPermission();
                } else {
                    Toast.makeText(this, "Please grant permissions to access mic", Toast.LENGTH_LONG).show();
                }
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

    }

    public void speechText(final String text){
        Log.e("ucapkan", "speechText: "+text );
        if (TextUtils.isEmpty(text)){
            Toast.makeText(context, "Tidak dimengerti, coba ucapkan lagi", Toast.LENGTH_SHORT).show();
        }else{
            setUpSpeak(text);
            btnPlay.setVisibility(View.VISIBLE);
            btnPlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setUpSpeak(text);
                }
            });
        }
    }


}
