package andriboom.tbotubes.assets;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import andriboom.tbotubes.HomeActivity;
import andriboom.tbotubes.R;

import static andriboom.tbotubes.assets.AppData.yandexKey;

public class TranslateBackgroundTask extends AsyncTask<String, Void, String> {
    //Declare Context
    @SuppressLint("StaticFieldLeak")
    private Context ctx;
    @SuppressLint("StaticFieldLeak")
    private HomeActivity homeActivity;
    private ProgressDialog loadingDialog;
    //Set Context
    public TranslateBackgroundTask(Context ctx, HomeActivity homeActivity){
        this.ctx = ctx;
        this.homeActivity = homeActivity;

        loadingDialog = new ProgressDialog(ctx);
        loadingDialog.setTitle(null);
        loadingDialog.setMessage("Memproses text....");
        loadingDialog.setCancelable(false);
    }

    public TranslateBackgroundTask(Context context, HomeActivity homeActivity, boolean iden) {
    }

    @Override
    protected String doInBackground(String... params) {
        //String variables
        String textToBeTranslated = params[0];
        String languagePair = params[1];

        String jsonString;

        try {
            //Set up the translation call URL
            String yandexUrl = "https://translate.yandex.net/api/v1.5/tr.json/translate?key=" + yandexKey
                    + "&text=" + textToBeTranslated + "&lang=" + languagePair;
            URL yandexTranslateURL = null;
            try {
                yandexTranslateURL = new URL(yandexUrl);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            //Set Http Conncection, Input Stream, and Buffered Reader
            HttpURLConnection httpJsonConnection = (HttpURLConnection) yandexTranslateURL.openConnection();
            InputStream inputStream = httpJsonConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            //Set string builder and insert retrieved JSON result into it
            StringBuilder jsonStringBuilder = new StringBuilder();
            while ((jsonString = bufferedReader.readLine()) != null) {
                jsonStringBuilder.append(jsonString + "\n");
            }

            //Close and disconnect
            bufferedReader.close();
            inputStream.close();
            httpJsonConnection.disconnect();

            //Making result human readable
            String resultString = jsonStringBuilder.toString().trim();
            //Getting the characters between [ and ]
            resultString = resultString.substring(resultString.indexOf('[')+1);
            resultString = resultString.substring(0,resultString.indexOf("]"));
            //Getting the characters between " and "
            resultString = resultString.substring(resultString.indexOf("\"")+1);
            resultString = resultString.substring(0,resultString.indexOf("\""));

            Log.d("Translation Result:", resultString);
            return resultString;
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        loadingDialog.show();
    }

    @Override
    protected void onPostExecute(String result) {
        if (!result.isEmpty()){
            homeActivity.speechText(result);
        }else{
            Toast.makeText(ctx, "Tidak dimengerti, coba ulangi lagi", Toast.LENGTH_SHORT).show();
        }
        if (loadingDialog.isShowing()){
            loadingDialog.dismiss();
        }
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }
}
