package andriboom.tbotubes.assets;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiHelper {
    @FormUrlEncoded
    @POST("api/v1.5/tr.json/translate")
    Call<TranslatedText> getTranslation(@Query("key") String APIKey,
                                        @Field("text") String textToTranslate,
                                        @Query("lang") String lang);
}
